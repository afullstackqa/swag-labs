import inventoryComponent from 'tests/pages/shop-page/inventory.comp';
import BasePage from 'pages/page';
import { PAGE_DATA } from 'fixtures/static';

class ShopPage extends BasePage {
  inventory = inventoryComponent;
  urlPath = PAGE_DATA.shop.urlPath;

  open(): void {
    browser.url(this.urlPath);
  }

  private get productSortDropdown(): WebdriverIO.Element {
    return $('.product_sort_container');
  }

  productSortDropdownValue(): string {
    return this.productSortDropdown.getValue();
  }

  selectProductSortOption(sortValue: string): void {
    this.productSortDropdown.selectByAttribute('value', sortValue);
  }
}

export default new ShopPage();
