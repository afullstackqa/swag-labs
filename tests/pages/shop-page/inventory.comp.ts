import itemCardComponent from 'tests/pages/shared/components/item-card';

interface ProductInfoInterface {
  item: WebdriverIO.Element;
  itemId: number;
  itemName: string;
  itemPrice: number;
  cartButton: WebdriverIO.Element;
}

class InventoryComponent {
  private itemCard = itemCardComponent;

  private get productList(): WebdriverIO.ElementArray {
    return $$('.inventory_item');
  }

  productInfo() {
    return this.productList.map((product) => {
      const productInfo: ProductInfoInterface = {
        item: product,
        itemId: this.itemCard.itemId(product, '.inventory_item_label>a'),
        itemName: this.itemCard.itemName(product, '.inventory_item_name'),
        itemPrice: this.itemCard.itemPrice(
          product,
          '.inventory_item_price',
          '$'
        ),
        cartButton: this.itemCard.cartButton(product, '.btn_inventory'),
      };
      return productInfo;
    });
  }

  productPrices() {
    return this.productInfo().map(({ itemPrice }) => itemPrice);
  }
}

export default new InventoryComponent();
