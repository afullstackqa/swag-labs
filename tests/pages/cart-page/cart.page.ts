import BasePage from 'pages/page';
import cartItem from 'pages/cart-page/cart-item.comp';
import { PAGE_DATA } from 'fixtures/static';

class CartPage extends BasePage {
  cartItem = cartItem;
  urlPath = PAGE_DATA.cart.urlPath;

  open(): void {
    browser.url(this.urlPath);
  }

  private get checkoutButton(): WebdriverIO.Element {
    return $('.checkout_button');
  }

  clickOnCheckoutButton(): void {
    this.checkoutButton.click();
  }
}
export default new CartPage();
