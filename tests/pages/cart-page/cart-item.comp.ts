import itemCardComponent from 'tests/pages/shared/components/item-card';

interface ProductInfoInterface {
  item: WebdriverIO.Element;
  itemId: number;
  itemQuantity: number;
  itemName: string;
  itemPrice: number;
  cartButton: WebdriverIO.Element;
}
class CartItemComponent {
  private itemCard = itemCardComponent;

  private get productList(): WebdriverIO.ElementArray {
    return $$('.cart_item');
  }

  productInfo() {
    return this.productList.map((product) => {
      const productInfo: ProductInfoInterface = {
        item: product,
        itemId: this.itemCard.itemId(product, '.cart_item_label>a'),
        itemQuantity: parseInt(product.$('.cart_quantity').getText()),
        itemName: this.itemCard.itemName(product, '.inventory_item_name'),
        itemPrice: this.itemCard.itemPrice(
          product,
          '.inventory_item_price',
          '$'
        ),
        cartButton: this.itemCard.cartButton(product, '.cart_button'),
      };
      return productInfo;
    });
  }

  productPrices() {
    return this.productInfo().map(({ itemPrice }) => itemPrice);
  }
}

export default new CartItemComponent();
