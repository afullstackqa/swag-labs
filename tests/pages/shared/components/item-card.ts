class ItemCardComponent {
  itemId(product: WebdriverIO.Element, selector: string): number {
    const regex = /id=(\d+)/;
    const itemIdHrefValue = product.$(selector).getAttribute('href');
    const matchedItemId = itemIdHrefValue.match(regex);
    const itemId: number | null =
      matchedItemId !== null ? parseInt(matchedItemId[1]) : 0;

    return itemId;
  }

  itemName(product: WebdriverIO.Element, selector: string): string {
    return product.$(selector).getText();
  }

  itemPrice(
    product: WebdriverIO.Element,
    selector: string,
    currencySymbol?: string
  ): number {
    if (currencySymbol) {
      const itemPrice = product
        .$(selector)
        .getText()
        .replace(currencySymbol, '');
      return parseFloat(itemPrice);
    }
    return parseFloat(product.$(selector).getText());
  }

  cartButton(
    product: WebdriverIO.Element,
    selector: string
  ): WebdriverIO.Element {
    return product.$(selector);
  }
}

export default new ItemCardComponent();
