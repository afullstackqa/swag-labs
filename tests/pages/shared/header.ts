class HeaderComponent {
  private get shoppingCart(): WebdriverIO.Element {
    return $('#shopping_cart_container > a');
  }

  clickOnShoppingCart(): void {
    this.shoppingCart.click();
  }

  private get shoppingCartBadgeCounter(): WebdriverIO.Element {
    return $('#shopping_cart_container span');
  }

  shoppingCartBadgeCounterValue(): number {
    return parseInt(this.shoppingCartBadgeCounter.getText());
  }
}

export default new HeaderComponent();
