import BasePage from 'pages/page';
import { PAGE_DATA } from 'fixtures/static';
class LoginPage extends BasePage {
  urlPath = PAGE_DATA.login.urlPath;

  open(): void {
    browser.url(this.urlPath);
  }

  private get usernameField(): WebdriverIO.Element {
    return $('[data-test=username]');
  }

  fillInUsername(username: string): void {
    this.usernameField.setValue(username);
  }

  private get passwordField(): WebdriverIO.Element {
    return $('[data-test=password]');
  }

  fillInPassword(password: string): void {
    this.passwordField.setValue(password);
  }

  private get loginButton(): WebdriverIO.Element {
    return $('[value=LOGIN]');
  }

  clickOnLoginButton(): void {
    this.loginButton.click();
  }

  attemptToLogin(username: string, password: string): void {
    this.fillInUsername(username);
    this.fillInPassword(password);
    this.clickOnLoginButton();
  }
}

export default new LoginPage();
