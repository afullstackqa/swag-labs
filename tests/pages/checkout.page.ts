import BasePage from 'pages/page';
import { PAGE_DATA } from 'fixtures/static';

class CheckoutPage extends BasePage {
  urlPath = PAGE_DATA.login.urlPath;

  open(): void {
    browser.url(this.urlPath);
  }

  private get checkoutInfoContainer(): WebdriverIO.Element {
    return $('#checkout_info_container');
  }

  isCheckoutInfoContainerDisplayed(): boolean {
    return this.checkoutInfoContainer.isDisplayed();
  }
}

export default new CheckoutPage();
