export const composeAppUrl = (path: string): string => {
  return `${browser.config.baseUrl}${path}`;
};

export const isArraySortedDesc = (arr: any[]): boolean =>
  !!arr.reduce(
    (acc: number, curr: number) => acc !== null && acc >= curr && curr
  );

export const getLastNumberOfItemsFrom = (
  arr: any[],
  numberOfItem: number
): any[] => arr.slice(arr.length - numberOfItem);
