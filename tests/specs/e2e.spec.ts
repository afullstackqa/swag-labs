import { expect } from 'chai';
import { USER_DATA, INVENTORY_DATA, PAGE_DATA } from 'fixtures/static';
import loginPage from 'pages/login.page';
import shopPage from 'tests/pages/shop-page/shop.page';
import {
  composeAppUrl,
  isArraySortedDesc,
  getLastNumberOfItemsFrom,
} from 'tests/utils/helper';
import cartPage from 'tests/pages/cart-page/cart.page';
import checkoutPage from 'tests/pages/checkout.page';

describe('E2E: SWAG Shop flow', () => {
  let selectedProducts: any[] = [];

  before(() => {
    loginPage.open();
  });

  it(`Login to ${browser.config.baseUrl} using the ${USER_DATA.username} account`, () => {
    const { username, password } = USER_DATA;
    const { urlPath } = PAGE_DATA.shop;

    loginPage.attemptToLogin(username, password);

    const actualUrl = browser.getUrl();
    const expectedUrl = composeAppUrl(urlPath);
    expect(actualUrl).to.eq(expectedUrl);
  });

  it('Sort the products by Price (high to low)', () => {
    const { hilo } = INVENTORY_DATA.inventorySort;
    shopPage.selectProductSortOption(hilo);
    expect(shopPage.productSortDropdownValue()).to.eq(
      hilo,
      'The product sort dropdown value did not change'
    );

    const productPriceList = shopPage.inventory.productPrices();
    const isInventorySorted = isArraySortedDesc(productPriceList);
    expect(isInventorySorted).to.eq(
      true,
      'The products were not sorted by Price (high to low).'
    );
  });

  it('Add the two cheapest products to your basket', () => {
    const productList = shopPage.inventory.productInfo();
    const numberOfItems = 2;

    selectedProducts = getLastNumberOfItemsFrom(productList, numberOfItems);
    selectedProducts.forEach((product) => product.cartButton.click());

    const { remove } = INVENTORY_DATA.product.cartButtonState;
    selectedProducts.forEach((product) => {
      expect(product.cartButton.getText()).to.eq(
        remove,
        'The Cart Button did not change state'
      );
    });

    expect(shopPage.header.shoppingCartBadgeCounterValue()).to.eq(
      numberOfItems
    );
  });

  it('Open the basket', () => {
    const { urlPath } = PAGE_DATA.cart;

    shopPage.header.clickOnShoppingCart();

    const actualUrl = browser.getUrl();
    const expectedUrl = composeAppUrl(urlPath);
    expect(actualUrl).to.eq(expectedUrl);

    // order by id the items from cart and shop for comparison
    const actualProductsInTheCart = cartPage.cartItem
      .productInfo()
      .sort((a, b) => a.itemId - b.itemId);

    const expectedProductsFromTheShop = selectedProducts.sort(
      (a, b) => a.itemId - b.itemId
    );

    expect(actualProductsInTheCart.length).to.eq(
      expectedProductsFromTheShop.length,
      'There are wrong number of products in the Cart'
    );

    actualProductsInTheCart.forEach((product, i) => {
      expect(product.itemId).to.eq(
        expectedProductsFromTheShop[i].itemId,
        'Products in the Cart do not match selected products form the Shop'
      );
    });
  });

  it('Remove the cheapest product from your basket', () => {
    const cheapestItemInTheCart = cartPage.cartItem
      .productInfo()
      .reduce((acc, curr) => (acc.itemPrice <= curr.itemPrice ? acc : curr));
    cheapestItemInTheCart.cartButton.click();
    const actualProductsInTheCart = cartPage.cartItem.productInfo();
    expect(actualProductsInTheCart.length).to.eq(
      1,
      'There are one too many items in the Cart'
    );
    expect(cheapestItemInTheCart.item.isDisplayed()).to.eq(
      false,
      'The cheapest item has not been removed'
    );
  });

  it('Checkout', () => {
    const { urlPath } = PAGE_DATA.checkout;

    cartPage.clickOnCheckoutButton();

    const actualUrl = browser.getUrl();
    const expectedUrl = composeAppUrl(urlPath);
    expect(actualUrl).to.eq(expectedUrl);

    expect(checkoutPage.isCheckoutInfoContainerDisplayed()).to.eq(
      true,
      'The Checkout Info form is missing'
    );
  });
});
