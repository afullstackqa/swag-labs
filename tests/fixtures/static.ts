import appData from 'env';

// Pages Data
interface PageDataInterface {
  login: {
    urlPath: string;
  };
  shop: {
    urlPath: string;
  };
  cart: {
    urlPath: string;
  };
  checkout: {
    urlPath: string;
  };
}

export const PAGE_DATA: PageDataInterface = {
  login: { urlPath: '' },
  shop: { urlPath: 'inventory.html' },
  cart: { urlPath: 'cart.html' },
  checkout: { urlPath: 'checkout-step-one.html' },
};

// User Data
interface UserDataInterface {
  username: string;
  password: string;
}

export const USER_DATA: UserDataInterface = {
  username: appData.USERNAME,
  password: appData.PASSWORD,
};

// Inventory and Product Data
interface InventorySortInterface {
  az: string;
  za: string;
  lohi: string;
  hilo: string;
}

const INVENTORY_SORT_VALUES: InventorySortInterface = {
  az: 'az',
  za: 'za',
  lohi: 'lohi',
  hilo: 'hilo',
};

interface ProductDataInterface {
  cartButtonState: {
    add: string;
    remove: string;
  };
}
interface InventoryDataInterface {
  inventorySort: InventorySortInterface;
  product: ProductDataInterface;
}

/**
 * Implementation of TEXT data would need to change
 * once different languages are introduced
 * it would be better to have it pull from API
 */
export const INVENTORY_DATA: InventoryDataInterface = {
  inventorySort: INVENTORY_SORT_VALUES,
  product: {
    cartButtonState: {
      add: 'ADD TO CART',
      remove: 'REMOVE',
    },
  },
};
