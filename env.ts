import envalid from 'envalid';

export default envalid.cleanEnv(
  process.env,
  {
    BASE_URL: envalid.url({ default: 'https://www.saucedemo.com/' }),
    USERNAME: envalid.str({ default: 'fake_user' }),
    PASSWORD: envalid.str({ default: 'fake_pwd' }),
  },
  { strict: true }
);
