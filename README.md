# SWAG LABS 🛍

## Quick Start ⏭

* Ensure you have [nodejs installed](https://nodejs.org/en/) for your environment
* Ensure you have [yarn installed](https://yarnpkg.com/getting-started/install) for your environment
* Clone this repo `git clone ...`
* Create a `.env` file in the root directory
* Add following `key/value` pair to the `.env` file
    * `BASE_URL=https://url/` *optional - it's hard coded by default*
    * `USERNAME=testusername` *required - see web app home page for clues*
    * `PASSWORD=testpassword` *required - see web app home page for clues*
* In a terminal of your choice
    * Run `yarn install` to install all dependencies
* To run all specs execute following command `yarn run test`

## Objectives 🎯

Build a framework for automated checks for the following web app https://www.saucedemo.com/.

1. Login to https://www.saucedemo.com/ using the "standard_user" account
2. Sort the products by Price (high to low)
3. Add the two cheapest products to your basket
4. Open the basket
5. Remove the cheapest product from your basket
6. Checkout
7. Finish on the page where you need to enter your name and postal code

### AC
>* Demonstrated JavaScript or TypeScript and node.js knowledge (the more modern syntax the better!)
>* The test code is concise and easy to understand
>* The test effectively validates the intended functionality
>* The framework is well structured
>* The framework can be extended in an intuitive way


## Assumptions and Thoughts 💭
This is great challenge that really forces you to wear multiple hats and think from different perspectives.

There are some areas where I have had to make educated assumption. 
Where I would normally have either a 3 amigos or quick informal chat with a team member.
One of the example of this can be found:
* For `static` fixtures I've added English translation of values for some elements for assertion use. I would need to clarify whether the stakeholder is planning to introduce languages, and if we want to assert the values etc
* The order in which items are displayed in the cart - I would confirm the requirements and add an assertion for it.
* The currency symbol is missing in the cart - I would confirm if this is an issue (logic dictates that it is)
* For several elements I've had to use different locator strategies based on what was available. I would normally either request or add myself a unique attribute such as `data-test` to be added for required elements
* Ideally it would be good to fetch dynamic data from API and feed that in to the e2e checks.